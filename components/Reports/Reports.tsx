import React, { useEffect, useState } from "react";
import classnames from "classnames";
import { Chart } from "react-google-charts";
import {
  IHarvest,
  IOrchard,
  IPercentageData,
  IVariety,
} from "./Reports.interface";
import {
  formatMoney,
  getChartData,
  getfilteredData,
  getPercentageData,
  getStaffs,
  getTotal,
  getTotalCost,
  getVarieties,
} from "../../helper/Report";
import Select from "../Form/Select";
import DatePicker from "../Form/DatePicker";
import styles from "./Reports.module.scss";
import { TypeDateRange } from "../Form/Form.interface";

const Reports = ({
  harvest,
  orchards,
  varieties,
}: {
  harvest: IHarvest[];
  orchards: IOrchard[];
  varieties: IVariety[];
}) => {
  const [filteredData, setFilteredData] = useState<IHarvest[]>(harvest);
  const [percentageType, setPercentageType] = useState("varieties");
  const [chartData, setChartData] = useState<IPercentageData[]>();
  const [dateRange, setDateRange] = useState<TypeDateRange>();
  const [filterOrChardId, setFilterOrChardId] = useState("");

  useEffect(() => {
    const typeData = percentageType === "varieties" ? varieties : orchards;

    const data = getPercentageData(
      typeData,
      percentageType === "varieties" ? "varietyId" : "orchardId",
      filteredData
    );
    setChartData(data);
  }, [filteredData, percentageType]);

  useEffect(() => {
    setFilteredData(getfilteredData(harvest, dateRange, filterOrChardId));
  }, [dateRange, filterOrChardId]);

  const handleChangePercentageType = (id: string) => {
    setPercentageType(id);
  };

  const percentageTab = [
    { id: "varieties", name: "Varieties" },
    { id: "orchards", name: "Orchards" },
  ];

  return (
    <>
      <div className={classnames(styles["report__block"])}>
        <div className="flex justify-start gap-12 mb-9">
          <DatePicker
            value={dateRange}
            onChange={setDateRange}
            label="Date Range"
          />
          <Select
            icon="hectre-icon-filter"
            options={orchards}
            onChange={(orchardId) => setFilterOrChardId(orchardId)}
            label="Orchards"
          />
        </div>
        <div className="grid grid-cols-6 border-t-2 border-red bg-grey-6 p-5 text-center">
          <div className="">
            <div className={styles["report__statistic-title"]}>Total Bins</div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-bins"
            >
              {getTotal(filteredData, "numberOfBins")}
            </div>
          </div>
          <div className="">
            <div className={styles["report__statistic-title"]}>
              Total Varieties
            </div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-varieties"
            >
              {getVarieties(filteredData)}
            </div>
          </div>
          <div className="">
            <div className={styles["report__statistic-title"]}>Total Staff</div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-staffs"
            >
              {getStaffs(filteredData)}
            </div>
          </div>
          <div className="">
            <div className={styles["report__statistic-title"]}>
              Total Working Hours
            </div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-hours-worked"
            >
              {formatMoney(getTotal(filteredData, "hoursWorked"))}
            </div>
          </div>
          <div className="">
            <div className={styles["report__statistic-title"]}>
              Average Rate
            </div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-rate"
            >
              {filteredData.length > 0
                ? formatMoney(
                    getTotal(filteredData, "payRatePerHour") /
                      filteredData.length
                  )
                : 0}
            </div>
          </div>
          <div className="">
            <div className={styles["report__statistic-title"]}>
              Total Labor Cost
            </div>
            <div
              className={styles["report__statistic-number"]}
              data-testid="statistic-cost"
            >
              {formatMoney(getTotalCost(filteredData))}
            </div>
          </div>
        </div>
      </div>
      <div className={classnames(styles["report__block"], "pl-10")}>
        <h2 className="uppercase text-2xl font-semibold mb-6">Percentage</h2>
        <ul>
          {percentageTab.map((item) => {
            return (
              <li
                key={item.id}
                className={classnames(
                  styles["tab"],
                  item.id === percentageType && styles["tab--active"]
                )}
              >
                <button
                  className="uppercase"
                  onClick={() => handleChangePercentageType(item.id)}
                >
                  {item.name}
                </button>
              </li>
            );
          })}
        </ul>
        {chartData && chartData.length > 2 && (
          <div className="grid grid-cols-2 text-center">
            <div>
              <Chart
                chartType="PieChart"
                data={getChartData(chartData, "bins", dateRange)}
                options={{
                  tooltip: { isHtml: true },
                  legend: { position: "none" },
                }}
                width="100%"
                height="400px"
                legendToggle
              />
              <div className={styles["chart__title"]}>Production</div>
              <div className="text-sm">
                TOTAL: {getTotal(filteredData, "numberOfBins")} bins
              </div>
            </div>
            <div>
              <Chart
                chartType="PieChart"
                data={getChartData(chartData, "cost", dateRange)}
                options={{
                  tooltip: { isHtml: true },
                  legend: { position: "top" },
                }}
                width="100%"
                height="400px"
                legendToggle
              />
              <div className={styles["chart__title"]}>Cost</div>
              <div className="text-sm">
                TOTAL: $ {formatMoney(getTotalCost(filteredData))}
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Reports;
