import { render, screen } from "@testing-library/react";
import { composeStories } from "@storybook/testing-react";
import * as stories from "./Reports.stories";
const { Initial } = composeStories(stories);

describe("test statistic data", () => {
  beforeEach(() => {
    render(<Initial />);
  });

  it("should show correct bins", () => {
    const data = screen.getByTestId("statistic-bins");
    expect(data.textContent).toEqual("287");
  });

  it("should show correct varieties", () => {
    const data = screen.getByTestId("statistic-varieties");
    expect(data.textContent).toEqual("13");
  });

  it("should show correct staffs", () => {
    const data = screen.getByTestId("statistic-staffs");
    expect(data.textContent).toEqual("13");
  });

  it("should show correct hoursWorked", () => {
    const data = screen.getByTestId("statistic-hours-worked");
    expect(data.textContent).toEqual("590.26");
  });

  it("should show correct rate", () => {
    const data = screen.getByTestId("statistic-rate");
    expect(data.textContent).toEqual("8.21");
  });

  it("should show correct cost", () => {
    const data = screen.getByTestId("statistic-cost");
    expect(data.textContent).toEqual("4,845.64");
  });
});
