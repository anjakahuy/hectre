export interface IHarvest {
  id: string;
  userId: string;
  varietyId: string;
  orchardId: string;
  pickingDate: string;
  numberOfBins: number;
  hoursWorked: number;
  payRatePerHour: number;
}

export interface IOrchard {
  id: string;
  name: string;
}

export interface IVariety extends IOrchard {}

export interface IPercentageData extends IOrchard {
  bins: number;
  cost: number;
}
