declare module "@wojtekmaj/react-daterange-picker" {
  import * as React from "react";
  import { TypeDateRange } from "./Form.interface";
  export interface Props {
    value: TypeDateRange;
    onChange: (value: Date[]) => void;
    calendarIcon: JSX.Element;
    format: string;
  }

  export default class DateRangePicker extends React.Component<Props, any> {}
}
