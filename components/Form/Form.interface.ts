export interface ISelect {
  options: { id: string; name: string }[];
  onChange: (value: string) => void;
  label: string;
  icon?: string;
}

export interface IDatePicker {
  value: TypeDateRange;
  onChange: (value: Date[]) => void;
  label: string;
}

export type TypeDateRange = Date[] | undefined;
