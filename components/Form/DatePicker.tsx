import React from "react";
import DateRangePicker from "../DateRangePicker/DateRangePicker";
import { IDatePicker } from "./Form.interface";
import styles from "./Form.module.scss";
import classnames from "classnames";

const DatePicker = ({ value, onChange, label }: IDatePicker) => {
  return (
    <div
      className={classnames(
        "w-72",
        styles["form__control"],
        styles["form__control-datepicker"]
      )}
    >
      <label className={styles["form__label"]}>{label}</label>
      <DateRangePicker
        onChange={onChange}
        value={value}
        calendarIcon={<em className="hectre-icon-calendar" />}
        format="MM-dd-y"
      />
    </div>
  );
};

export default DatePicker;
