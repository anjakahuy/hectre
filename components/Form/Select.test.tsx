import { render, screen, fireEvent } from "@testing-library/react";
import Select from "./Select";
const options = [
  {
    id: "b6dbb181-48ff-4ff7-b395-7fa8dcc146c4",
    name: "Andromeda",
  },
  {
    id: "4eb58296-5eda-49a1-9831-5838e52bc4dd",
    name: "Polaris",
  },
  {
    id: "1ba383ea-e9f0-4c35-8f01-ed88c7eeaf2a",
    name: "Sirius",
  },
  {
    id: "7ba12acf-95b3-4c8d-80ca-4c2b562971cc",
    name: "Vega",
  },
  {
    id: "ef71e690-4bbb-4684-9ebb-4b8fcd23eceb",
    name: "Pleiades",
  },
  {
    id: "34a76a76-23c6-40dd-b8c7-b822063f17b1",
    name: "Antares",
  },
  {
    id: "c9ce5f49-a33b-467e-b602-41fe335a6fe5",
    name: "Canopus",
  },
];
const mockCallback = jest.fn((x) => x);

test("change select", () => {
  render(
    <Select
      icon="hectre-icon-filter"
      options={options}
      onChange={(orchardId) => mockCallback(orchardId)}
      label="Orchards"
    />
  );

  fireEvent.change(screen.getByTestId("select"), {
    target: { value: "ef71e690-4bbb-4684-9ebb-4b8fcd23eceb" },
  });

  expect(mockCallback).toHaveBeenCalledWith(
    "ef71e690-4bbb-4684-9ebb-4b8fcd23eceb"
  );
});
