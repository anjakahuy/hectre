import React from "react";
import { ISelect } from "./Form.interface";
import styles from "./Form.module.scss";
import classnames from "classnames";

const Select = ({ options, onChange, label, icon }: ISelect) => {
  return (
    <div className="flex items-center">
      {icon && <em className={classnames(icon, "text-grey-3")} />}
      <div className={classnames("ml-4 w-60", styles["form__control"])}>
        <label className={styles["form__label"]}>{label}</label>
        <select
          className="w-full"
          data-testid="select"
          onChange={(e) => onChange(e.target.value)}
        >
          <option value="">All</option>
          {options.map((item) => {
            return (
              <option key={item.id} value={item.id}>
                {item.name}
              </option>
            );
          })}
        </select>
      </div>
    </div>
  );
};

export default Select;
