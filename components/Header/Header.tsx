import React, { useState } from "react";
import { IHeader } from "./Header.interface";
import Image from "next/image";
import { getFirstLetters } from "../../helper/String";
import { logOutUrl } from "../../helper/AwsCognito";

const Header = ({ name, role }: IHeader) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const handleLogout = () => {
    window.location.replace(logOutUrl());
  };
  return (
    <div className="bg-white fixed top-0 w-full pl-16 shadow-lg z-10">
      <div className="px-8 py-4 flex justify-between items-center">
        <Image src="/logo.png" alt="Hectre Logo" width="100" height="30" />
        <div>
          {name && (
            <div className="relative inline-block text-left">
              <div className="flex items-center">
                <div className="bg-gold text-white inline-block rounded-full p-2 w-10 h-10">
                  {getFirstLetters(name)}
                </div>
                <button
                  type="button"
                  className="inline-flex justify-center px-4 py-2 bg-white text-sm font-medium gap-3"
                  id="menu-button"
                  aria-expanded="true"
                  aria-haspopup="true"
                  onClick={() => setIsOpenMenu(!isOpenMenu)}
                >
                  <div>
                    <span className="text-grey-1">{name}</span>
                    <br />
                    <span className="text-grey-4 text-xs">{role}</span>
                  </div>
                  <em className="hectre-icon-arrow-down text-grey-3" />
                </button>
              </div>
              {isOpenMenu && (
                <div
                  className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                  role="menu"
                  aria-orientation="vertical"
                  aria-labelledby="menu-button"
                  tabIndex={-1}
                >
                  <div className="py-1" role="none">
                    <button
                      onClick={() => handleLogout()}
                      className=" text-left block px-4 py-2 text-sm w-full"
                    >
                      Logout
                    </button>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Header;
