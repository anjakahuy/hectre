export interface IHeader {
  name?: string;
  role?: string;
}
