import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";

import Header from "./Header";

export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Header",
  component: Header,
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />;

export const Initial = Template.bind({});

Initial.args = {
  name: "",
  role: "",
};

export const Logged = Template.bind({});

Logged.args = {
  name: "Huy Nguyen",
  role: "Developer",
};
