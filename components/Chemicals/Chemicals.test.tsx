import { render, screen, fireEvent } from "@testing-library/react";
import { composeStories } from "@storybook/testing-react";
import * as stories from "./Chemicals.stories";

const { NoResult, Pagination, NoPagination } = composeStories(stories);

describe("Chemicals without data", () => {
  beforeEach(() => {
    render(<NoResult />);
  });
  it("should show no record", () => {
    const noRecordText = screen.getByText("There is no record");
    expect(noRecordText).toBeInTheDocument();
    const numberItems = screen.getByTestId("number-items");
    expect(parseInt(`${numberItems.textContent}`)).toEqual(0);
  });

  it("should not show pagination and perpage", () => {
    const pagination = screen.queryByTestId("pagination");
    const perpage = screen.queryByTestId("perpage");
    expect(pagination).not.toBeInTheDocument();
    expect(perpage).not.toBeInTheDocument();
  });
});

describe("Chemicals with number of data less than number of perpage", () => {
  beforeEach(() => {
    render(<NoPagination />);
  });
  it("should show correct number of chemicals at header", () => {
    const numberItems = screen.getByTestId("number-items");
    expect(parseInt(`${numberItems.textContent}`)).toEqual(
      NoPagination.args?.items?.length
    );
  });

  it("should show correct table rows", () => {
    const tableBody = screen.getByTestId("table-body");
    expect(tableBody.children.length).toEqual(NoPagination.args?.items?.length);
  });

  it("should not show pagination but show perpage", () => {
    const pagination = screen.queryByTestId("pagination");
    const perpage = screen.queryByTestId("perpage");
    expect(pagination).not.toBeInTheDocument();
    expect(perpage).toBeInTheDocument();
  });
});

describe("Chemicals with data and pagination", () => {
  beforeEach(() => {
    render(<Pagination />);
  });

  it("should show correct number of chemicals at header", () => {
    const numberItems = screen.getByTestId("number-items");
    expect(parseInt(`${numberItems.textContent}`)).toEqual(
      Pagination.args?.items?.length
    );
  });

  it("should show 10 rows at initial", () => {
    const tableBody = screen.getByTestId("table-body");
    expect(tableBody.children.length).toEqual(10);
  });

  it("should show pagination and perpage", () => {
    const pagination = screen.queryByTestId("pagination");
    const perpage = screen.queryByTestId("perpage");
    expect(pagination).toBeInTheDocument();
    expect(perpage).toBeInTheDocument();
  });

  it("should change rows when changing perpage", () => {
    const selectPerpage = screen.getByTestId("select-perpage");
    fireEvent.change(selectPerpage, { target: { value: 20 } });
    const tableBody = screen.getByTestId("table-body");
    expect(tableBody.children.length).toEqual(20);
  });
});
