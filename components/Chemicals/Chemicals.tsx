import React, { useEffect, useState } from "react";
import Pagination from "../Pagination/Pagination";
import { IChemical } from "./Chemicals.interface";
import styles from "./Chemicals.module.scss";
import classnames from "classnames";
const Chemicals = ({ items }: { items: IChemical[] }) => {
  const [perPage, setPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [paginatedPosts, setPaginatedPosts] = useState<IChemical[]>([]);
  useEffect(() => {
    const currentPageNumber = currentPage * perPage - perPage;
    let paginatedPosts = [...items];
    paginatedPosts = paginatedPosts.splice(currentPageNumber, perPage);
    setPaginatedPosts(paginatedPosts);
  }, [perPage, currentPage, items]);

  return (
    <>
      <div className="chemicals bg-white">
        <div className="chemicals__header py-6 pl-20 border-b-2 border-red">
          <span className="text-xl uppercase inline-block pr-4">Chemicals</span>{" "}
          <span className="text-grey-3">
            There are <span data-testid="number-items">{items.length}</span>{" "}
            chemicals in total{" "}
          </span>
        </div>
        <div className={classnames(styles["chemicals__table"], "px-10")}>
          <table className="w-full">
            <thead>
              <tr>
                <th>Chemical Type</th>
                <th>Activeingredient</th>
                <th>Name</th>
                <th>Phi (days)</th>
              </tr>
            </thead>
            <tbody data-testid="table-body">
              {items.length > 0 ? (
                <>
                  {paginatedPosts.map((item, index) => (
                    <tr key={`chemial_${index}`}>
                      <td>{item.chemicalType}</td>
                      <td>{item.activeIngredient}</td>
                      <td>{item.name}</td>
                      <td>{item.phi}</td>
                    </tr>
                  ))}
                </>
              ) : (
                <tr>
                  <td colSpan={4}>There is no record</td>
                </tr>
              )}
            </tbody>
          </table>
          <div className="chemial__pagination">
            <Pagination
              totalPages={Math.ceil(items.length / perPage)}
              current={currentPage}
              onChangePage={(currentPage: number) =>
                setCurrentPage(currentPage)
              }
              onChangePerPage={(perPage: number) => setPerPage(perPage)}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Chemicals;
