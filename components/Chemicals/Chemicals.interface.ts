export interface IChemical {
  chemicalType: string;
  activeIngredient: string;
  name: string;
  phi: string;
}
