import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";

import SideBar from "./SideBar";

export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "SideBar",
  component: SideBar,
} as ComponentMeta<typeof SideBar>;

const Template: ComponentStory<typeof SideBar> = (args) => (
  <SideBar {...args} />
);

export const Initial = Template.bind({});

Initial.args = {
  menu: [
    { name: "Spray", icon: "hectre-icon-chemicals", url: "/" },
    {
      name: "Harvesting Report",
      icon: "hectre-icon-report",
      url: "/harvesting-report",
    },
  ],
};
