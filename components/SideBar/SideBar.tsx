import React, { useState } from "react";
import { IMenuItem } from "./SideBar.interface";
import Link from "next/link";
import classnames from "classnames";
import styles from "./SideBar.module.scss";

const SideBar = ({
  menu,
  currentPath,
}: {
  menu: IMenuItem[];
  currentPath: string;
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleToggleMenu = (
    e: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    e.preventDefault();
    setIsOpen(!isOpen);
  };
  return (
    <div
      className={classnames(styles["menu"], isOpen && styles["menu--isOpen"])}
    >
      <ul>
        <li className={classnames(styles["menu__item"])}>
          <a href="#" onClick={(e) => handleToggleMenu(e)}>
            <em className="hectre-icon-menu" />
          </a>
        </li>
        {menu.map((item, index) => {
          return (
            <li
              key={`menu_${index}`}
              className={classnames(
                styles["menu__item"],
                currentPath === item.url && styles["menu__item--active"]
              )}
            >
              <Link href={item.url}>
                <a>
                  <em className={item.icon} />
                  {isOpen && <span>{item.name}</span>}
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SideBar;
