import React from "react";
import classnames from "classnames";
import { IPagerItem } from "./Pagination.interface";
import styles from "./Pagination.module.scss";

const PagerItem = ({
  page,
  current,
  text,
  totalPages,
  onChange,
}: IPagerItem): JSX.Element => {
  const ariaLabelText =
    page === current
      ? `Page ${text} of ${totalPages} selected`
      : `Page ${text} of ${totalPages}`;
  return (
    <li
      className={classnames(
        styles["pager__item"],
        page === current && styles["pager__item--active"]
      )}
    >
      <button aria-label={ariaLabelText} onClick={() => onChange()}>
        {text}
      </button>
    </li>
  );
};

export default PagerItem;
