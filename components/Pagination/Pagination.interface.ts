export interface IPagination {
  totalPages: number;
  current: number;
  onChangePage: (page: number) => void;
  onChangePerPage: (perPage: number) => void;
}

export interface IPagerItem {
  current: number;
  page: number;
  onChange: () => void;
  text: number;
  totalPages: number;
}
