import React from "react";
import PagerItem from "./PagerItem";
import PagerBreak from "./PagerBreak";
import { IPagination } from "./Pagination.interface";
import styles from "./Pagination.module.scss";
import classnames from "classnames";

const Pagination = ({
  totalPages,
  current,
  onChangePage,
  onChangePerPage,
}: IPagination): JSX.Element => {
  const nextPrevThreshold = 7;

  const getRange = (start: number, end: number) =>
    Array(end - start + 1)
      .fill(null)
      .map((v, i) => i + start);

  const createPager = () => {
    const curr = current;
    const length = totalPages;
    const delta = length === nextPrevThreshold ? nextPrevThreshold : 3;
    const dotsRange = 3;
    const range = {
      start: Math.round(curr - delta / dotsRange) + 1,
      end: Math.round(curr + delta / dotsRange) + 1,
    };

    if (range.start - 1 === 1 || range.end + 1 === length) {
      range.start += 1;
      range.end += 1;
    }

    const createPages = (): (string | number)[] => {
      if (curr > delta && range.end <= length - delta) {
        return getRange(
          Math.min(range.start, length - delta),
          Math.min(range.end, length)
        );
      }
      if (curr > delta && range.end > length - delta && totalPages === 4) {
        return getRange(length - delta, length);
      }
      if (curr > delta && range.end > length - delta) {
        return getRange(length - delta - 1, length);
      }
      return getRange(1, Math.min(length, delta + 2));
    };

    let pages = createPages();

    const withDots = (value: number, pair: (string | number)[]) =>
      pages.length + 1 !== length ? pair : [value];

    if (pages[0] !== 1) {
      pages = withDots(1, [1, "..."]).concat(pages);
    }

    if (pages[pages.length - 1] < length) {
      pages = pages.concat(withDots(length, ["...", length]));
    }

    return pages;
  };

  const list = createPager().map((item) => {
    if (item === "...") {
      return <PagerBreak key={Math.random().toString()} />;
    }

    const number = parseInt(`${item}`);
    return (
      <PagerItem
        text={number}
        current={current}
        page={number}
        onChange={() => onChangePage(number)}
        key={Math.random().toString()}
        totalPages={totalPages}
      />
    );
  });

  const perPageOptions = [10, 20, 50, 100];
  return (
    <div className="flex justify-end items-center gap-10">
      {totalPages > 1 && (
        <nav className="pager py-8" data-testid="pagination" role="navigation">
          <ul className="pager__items js-pager__items flex gap-1 justify-end">
            <li
              className={classnames(
                styles["pager__item"],
                styles["pager__item--prev"]
              )}
            >
              <button
                aria-label="Go to previous Page"
                onClick={() => onChangePage(current !== 1 ? current - 1 : 1)}
              >
                <i className="hectre-icon-arrow-left"></i>
              </button>
            </li>

            {list}

            <li
              className={classnames(
                styles["pager__item"],
                styles["pager__item--next"]
              )}
            >
              <button
                aria-label="Go to next Page"
                onClick={() =>
                  onChangePage(
                    current !== totalPages ? current + 1 : totalPages
                  )
                }
              >
                <i className="hectre-icon-arrow-right"></i>
              </button>
            </li>
          </ul>
        </nav>
      )}
      {totalPages > 0 && (
        <div className="" data-testid="perpage">
          <span className="text-grey-3">Show records</span>
          <select
            data-testid="select-perpage"
            onChange={(e) => onChangePerPage(+e.target.value)}
          >
            {perPageOptions.map((item) => (
              <option key={`perPage_${item}`} value={item}>
                {item} rows
              </option>
            ))}
          </select>
        </div>
      )}
    </div>
  );
};

export default Pagination;
