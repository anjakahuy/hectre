import React from "react";

const PagerBreak = (): JSX.Element => {
  return (
    <li className="text-grey-4 mx-3">
      <span aria-hidden="true">
        <i className="hectre-icon-threedots"></i>
      </span>
    </li>
  );
};

export default PagerBreak;
