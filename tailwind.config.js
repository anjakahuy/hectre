module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      red: '#DF1D00',
      white: '#fff',
      gold: '#DB9200',
      grey: {
        DEFAULT: '#333333',
        1: '#4f4f4f',
        2: '#E5E5E5',
        3: '#828282',
        4: '#BDBDBD',
        5: '#E0E0E0',
        6: '#F9F9F9',
      }
    },
    extend: {
      borderWidth: {
        3: "3px"
      }
    },
  },
  plugins: [],
}
