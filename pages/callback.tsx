import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { getToken, getUserInfo, authUrl } from "../helper/AwsCognito";
import { withDashBoardLayout } from "../HOC/layouts/DashBoard";
import { setLogin } from "../helper/Cookie";

const Callback = () => {
  const [isFalse, setIsFalse] = useState(false);
  const router = useRouter();
  const { code } = router.query;
  useEffect(() => {
    const getData = async () => {
      const token = await getToken(`${code}`);
      if (token) {
        const userData = await getUserInfo(token.access_token);
        setLogin(token, userData);
        router.push("/harvesting-report");
      } else {
        setIsFalse(true);
      }
    };
    if (code) {
      getData();
    }
  }, [code]);

  const handleLogin = () => {
    window.location.replace(authUrl());
  };
  return (
    <>
      {isFalse ? (
        <div>
          Please try to login again with{" "}
          <button onClick={() => handleLogin()}>AWS Cognito</button>
        </div>
      ) : (
        <div>Verifying data...</div>
      )}
    </>
  );
};

export default withDashBoardLayout(Callback);
