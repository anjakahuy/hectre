import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { setLogout } from "../helper/Cookie";

const Signout = () => {
  const router = useRouter();
  useEffect(() => {
    setLogout();
    router.push("/");
  }, []);

  return <div>Logging out...</div>;
};

export default Signout;
