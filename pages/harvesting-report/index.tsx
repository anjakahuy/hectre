import React from "react";
import { withDashBoardLayout } from "../../HOC/layouts/DashBoard";
import Reports from "../../components/Reports/Reports";
import { useQuery } from "react-query";
import { Http } from "../../helper/Http";
import { withAuthenticated } from "../../HOC/auth/Auth";
import { authUrl } from "../../helper/AwsCognito";

const HavestingReport = () => {
  const harvestQuery = useQuery("harvest", () => Http("harvest"));
  const varietiesQuery = useQuery("varieties", () => Http("varieties"));
  const orchardsQuery = useQuery("orchards", () => Http("orchards"));

  if (
    harvestQuery.isLoading ||
    varietiesQuery.isLoading ||
    orchardsQuery.isLoading
  ) {
    return <span>Loading...</span>;
  }

  if (harvestQuery.isError || varietiesQuery.isError || orchardsQuery.isError) {
    window.location.replace(authUrl());
  }

  return (
    <>
      <Reports
        harvest={harvestQuery.data}
        varieties={varietiesQuery.data}
        orchards={orchardsQuery.data}
      />
    </>
  );
};

export default withAuthenticated(withDashBoardLayout(HavestingReport));
