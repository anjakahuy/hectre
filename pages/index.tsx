import { useEffect, useState } from "react";
import Chemicals from "../components/Chemicals/Chemicals";
import { IChemical } from "../components/Chemicals/Chemicals.interface";
import { Http } from "../helper/Http";
import { withDashBoardLayout } from "../HOC/layouts/DashBoard";
import { useQuery } from "react-query";
const Home = () => {
  const { isLoading, isError, data, error } = useQuery("todos", () =>
    Http("chemicals")
  );

  if (isLoading) {
    return <span>Loading...</span>;
  }

  if (isError) {
    return <span>Error: {error.message}</span>;
  }

  return <Chemicals items={data} />;
};

export default withDashBoardLayout(Home);
