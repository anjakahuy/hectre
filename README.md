## Description

This project is built with the NextJS, TypeScript.

## Purpose

This project was created to demonstrate my skills working with React.js

## Features

- Auth0 Login with AWS Cognito
- Chemicals table
- Report dashboard

## Configuration

- Create .env.local file

```sh
cp .env.example .env
```

## Running production

```sh
yarn install
yarn build
yarn start
```

Go to: http://localhost:4000

## Running development

```sh
yarn install
yarn dev
```

Go to: http://localhost:4000

## Running storybook

```sh
yarn install
yarn storybook
```

Go to: http://localhost:6006

## Testing
```sh
yarn test
```

## Linting

```sh
yarn run lint
yarn run lint:fix
yarn run format
```
## Genrerate icon font from svg

```sh
copy svg files to ./assets/icons
yarn font
```

## Libraries used

nextjs, tailwindcss, axios, typescript, react-daterange-picker
