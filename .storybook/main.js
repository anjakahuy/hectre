// .storybook/main.js
var webpack = require("webpack");

module.exports = {
  plugins: {
    tailwindcss: {},
    "postcss-preset-env": {
      autoprefixer: {
        flexbox: "no-2009",
      },
      stage: 3,
      features: {
        "custom-properties": false,
      },
    },
  },
  stories: [
    // Paths to the story files
    "../components/**/*.stories.@(js|mdx|jsx|tsx)",
  ],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /^((?!module).)*\.scss$/,
      use: [
        "style-loader",
        "css-loader",
        "postcss-loader",
        // Add the sass loader to process scss files
        "sass-loader",
      ],
    });
    config.module.rules.push({
      test: /\module\.scss/,
      use: [
        "style-loader",
        {
          loader: 'css-loader',
          options: {
            modules: true
          }
        },
        "postcss-loader",
        // Add the sass loader to process scss files
        "sass-loader",
      ],
    });

    return config;
  },
};
