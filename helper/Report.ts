import {
  IHarvest,
  IOrchard,
  IPercentageData,
} from "../components/Reports/Reports.interface";
import moment from "moment";
import { TypeDateRange } from "../components/Form/Form.interface";

export const getStaffs = (data: IHarvest[]) => {
  var tempResult: { [key: string]: string } = {};

  for (let { userId } of data) tempResult[userId] = userId;

  return Object.values(tempResult).length;
};

export const getVarieties = (data: IHarvest[]) => {
  var tempResult: { [key: string]: string } = {};

  for (let { varietyId } of data) tempResult[varietyId] = varietyId;

  return Object.values(tempResult).length;
};

export const getTotalCost = (data: IHarvest[]): number => {
  return data.reduce(
    (total, data) => total + data.hoursWorked * data.payRatePerHour,
    0
  );
};

export const getTotal = (
  data: IHarvest[],
  field: "numberOfBins" | "hoursWorked" | "payRatePerHour"
) => {
  return data.reduce((total: number, data: IHarvest) => total + data[field], 0);
};

export const getPercentageData = (
  data: IOrchard[],
  dataId: "varietyId" | "orchardId",
  filteredData: IHarvest[]
) => {
  const percentageData: IPercentageData[] = [];
  data.forEach((item) => {
    const percentageFiltered = getfilteredData(
      filteredData,
      undefined,
      dataId === "orchardId" ? item.id : "",
      dataId === "varietyId" ? item.id : ""
    );
    percentageData.push({
      ...item,
      bins: getTotal(percentageFiltered, "numberOfBins"),
      cost: getTotalCost(percentageFiltered),
    });
  });
  return percentageData;
};

export type chartDataType = number | string | object;

export const getChartData = (
  data: IPercentageData[],
  type: "bins" | "cost",
  dateRange: TypeDateRange
) => {
  const chartData: chartDataType[][] = [
    ["Name", "Number", { role: "tooltip", type: "string", p: { html: true } }],
  ];
  let sign = "$ ";
  let decimalCount = 2;
  let unit = "";
  if (type === "bins") {
    sign = "";
    decimalCount = 0;
    unit = "bins";
  }
  data.forEach((item) => {
    chartData.push([
      item.name,
      item[type],
      `
      <div class="text-left w-44 p-1">
      <div class="flex items-center gap-2">
        <em class="hectre-icon-calendar text-grey-3"></em>
        <span class="font-semibold">${renderRangeDate(dateRange)} </span>
      </div>
      <span class="text-red font-semibold">${item.name} ${formatMoney(
        item[type],
        sign,
        decimalCount
      )} </span>${unit}
      </div>
      `,
    ]);
  });
  return chartData;
};

const renderRangeDate = (dateRange: TypeDateRange) => {
  if (!dateRange || dateRange[0].getTime() === dateRange[1].getTime()) {
    return "All time";
  }
  return (
    moment(dateRange[0]).format("L") + " - " + moment(dateRange[1]).format("L")
  );
};

export const formatMoney = (
  amount: number,
  sign: string = "",
  decimalCount = 2,
  decimal = ".",
  thousands = ","
) => {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    let i = parseInt(amount.toFixed(decimalCount)).toString();
    let j = i.length > 3 ? i.length % 3 : 0;

    return (
      sign +
      (j ? i.substr(0, j) + thousands : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
      (decimalCount
        ? decimal +
          Math.abs(amount - parseFloat(i))
            .toFixed(decimalCount)
            .slice(2)
        : "")
    );
  } catch (e) {
    console.log(e);
  }
};

export const getfilteredData = (
  data: IHarvest[],
  dateRange?: TypeDateRange,
  orchardId?: string,
  varietyId?: string
) => {
  let filteredData: IHarvest[] = data;
  if (dateRange && dateRange[0].getTime() !== dateRange[1].getTime()) {
    filteredData = filteredData.filter(
      (item) =>
        dateRange[0].getTime() <= new Date(item.pickingDate).getTime() &&
        dateRange[1].getTime() >= new Date(item.pickingDate).getTime()
    );
  }

  if (orchardId) {
    filteredData = filteredData.filter((item) => item.orchardId === orchardId);
  }

  if (varietyId) {
    filteredData = filteredData.filter((item) => item.varietyId === varietyId);
  }

  return filteredData;
};
