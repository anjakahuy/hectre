import { getFirstLetters } from "./String";

test("should return a string", () => {
  expect(getFirstLetters("Nguyen")).toEqual("N");
  expect(getFirstLetters("Eddy Nguyen")).toEqual("EN");
  expect(getFirstLetters("Huy Nguyen Van")).toEqual("HNV");
  expect(getFirstLetters("")).toEqual("");
});
