import axios from "axios";

export const authUrl = () =>
  `${process.env.NEXT_PUBLIC_AWS_COGNITO_URL}/oauth2/authorize?response_type=${process.env.NEXT_PUBLIC_AWS_COGNITO_RESPONSE_TYPE}&client_id=${process.env.NEXT_PUBLIC_AWS_COGNITO_CLIENT_ID}&redirect_uri=${process.env.NEXT_PUBLIC_AWS_COGNITO_REDIRECT_URI}`;

export const getToken = async (code?: string, refreshToken?: string) => {
  const params = new URLSearchParams();
  params.append(
    "grant_type",
    refreshToken ? "refresh_token" : "authorization_code"
  );
  if (refreshToken) {
    params.append("refresh_token", `${refreshToken}`);
  } else {
    params.append("code", `${code}`);
    params.append(
      "redirect_uri",
      `${process.env.NEXT_PUBLIC_AWS_COGNITO_REDIRECT_URI}`
    );
  }
  params.append(
    "client_id",
    `${process.env.NEXT_PUBLIC_AWS_COGNITO_CLIENT_ID}`
  );

  try {
    const response = await axios.post(
      `${process.env.NEXT_PUBLIC_AWS_COGNITO_URL}/oauth2/token`,
      params,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
    return response.data;
  } catch (err) {
    return false;
  }
};

export const getUserInfo = async (token: string) => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_AWS_COGNITO_URL}/oauth2/userInfo`,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  } catch (err) {
    return false;
  }
};

export const logOutUrl = () =>
  `${process.env.NEXT_PUBLIC_AWS_COGNITO_URL}/logout
  client_id=${process.env.NEXT_PUBLIC_AWS_COGNITO_CLIENT_ID}&logout_uri=${process.env.NEXT_PUBLIC_AWS_COGNITO__LOGOUT_URI}`;
