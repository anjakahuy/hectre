import jsCookie from "js-cookie";

export const storeCookie = (name: string, value: any) => {
  jsCookie.set(name, JSON.stringify(value));
};

export const getCookie = (name: string) => {
  if (jsCookie.get(name)) {
    return JSON.parse(`${jsCookie.get(name)}`);
  }
};

export const clearCookie = (name: string) => {
  jsCookie.remove(name);
};

export const setLogin = (token: any, data: any) => {
  storeCookie("token", token);
  storeCookie("userData", data);
};

export const setLogout = () => {
  clearCookie("token");
  clearCookie("userData");
};
