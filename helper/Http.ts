import axios from "axios";
import jsCookie from "js-cookie";
import { getToken } from "./AwsCognito";
import { storeCookie } from "./Cookie";
const token = jsCookie.get("token")
  ? JSON.parse(`${jsCookie.get("token")}`)
  : false;

const axiosApiInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  timeout: 300000,
  headers: {
    "Content-Type": "application/json",
  },
});

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: `Bearer ${token.access_token}`,
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      try {
        const newToken = await getToken(undefined, token.refresh_token);
        if (newToken) {
          storeCookie("token", {
            ...newToken,
            refresh_token: token.refresh_token,
          });
          axiosApiInstance.defaults.headers.common["Authorization"] =
            "Bearer " + newToken.access_token;
          return axiosApiInstance(originalRequest);
        } else {
          return Promise.reject(error);
        }
      } catch (error) {
        return Promise.reject(error);
      }
    }
    return Promise.reject(error);
  }
);

export const Http = async (path: string) => {
  try {
    const res = await axiosApiInstance.get(`/api/${path}`);
    return res.data;
  } catch (err) {}
};
