
import React from 'react';

export const ArrowLeft = props => (
  <svg viewBox="0 0 20 20" {...props}><path d="M7.41 10.59L2.83 6L7.41 1.41L6 0L0 6L6 12L7.41 10.59Z" fillRule="evenodd" /></svg>
);
