
import React from 'react';

export const ArrowRight = props => (
  <svg viewBox="0 0 20 20" {...props}><path d="M0.589966 10.59L5.16997 6L0.589966 1.41L1.99997 0L7.99997 6L1.99997 12L0.589966 10.59Z" fillRule="evenodd" /></svg>
);
