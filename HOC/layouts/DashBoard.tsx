import React, { FunctionComponent, ReactNode } from "react";
import Header from "../../components/Header/Header";
import SideBar from "../../components/SideBar/SideBar";
import { useRouter } from "next/router";
import { getCookie } from "../../helper/Cookie";

const DashBoard = ({ children }: { children: ReactNode }) => {
  const router = useRouter();
  const menu = [
    { name: "Spray", icon: "hectre-icon-chemicals", url: "/" },
    {
      name: "Harvesting Report",
      icon: "hectre-icon-report",
      url: "/harvesting-report",
    },
  ];

  const user = getCookie("userData");

  return (
    <div>
      <SideBar menu={menu} currentPath={router.pathname} />
      <Header
        name={user ? `${user.family_name} ${user.given_name}` : undefined}
        role={user ? user.username : undefined}
      />
      <main role="main" className="pt-24 pl-16">
        {children}
      </main>
    </div>
  );
};

export const withDashBoardLayout = <T extends Record<string, unknown>>(
  Component: FunctionComponent<T>
) => {
  return function withLayoutComponent(props: T): JSX.Element {
    return (
      <DashBoard>
        <Component {...props} />
      </DashBoard>
    );
  };
};
