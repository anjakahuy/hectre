import { FunctionComponent, useEffect } from "react";
import jsCookie from "js-cookie";
import { authUrl } from "../../helper/AwsCognito";

export const withAuthenticated = <T extends Record<string, unknown>>(
  Component: FunctionComponent<T>
) => {
  return function ithComponent(props: T): JSX.Element {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      if (!jsCookie.get("token")) {
        window.location.href = authUrl();
      }
    }, []);
    return (
      <>
        <Component {...props} />
      </>
    );
  };
};
